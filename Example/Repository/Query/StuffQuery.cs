﻿using System.Collections.Generic;
using System.Linq;
using Example.Model;
using Example.Repository.Query.Interfaces;

namespace Example.Repository.Query
{
    public class StuffQuery : ReadRepository<Stuff>, IStuffQuery
    {
        public StuffQuery(DatabaseContext context) : base(context)
        {
        }

        /// <summary>
        /// Każda funkcja w Query powinna zwracać pojedynczy obiekt lub listę obiektów
        /// Nie powinna zajmować się niczym więcej poza wyciąganiem danych
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        public IList<Stuff> GetByStudent(Student student)
        {
            return Context.Stuffs.Where(x => x.StudentId == student.Id).ToList();
        }

    }
}