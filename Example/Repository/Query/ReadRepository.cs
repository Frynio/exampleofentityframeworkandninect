﻿using System.Collections.Generic;
using System.Linq;
using Example.Model;
using Example.Repository.Query.Interfaces;

namespace Example.Repository.Query
{
    public class ReadRepository<T> : IReadRepository<T> where T: Entity
    {
        protected readonly DatabaseContext Context;
        public ReadRepository(DatabaseContext context)
        {
            Context = context;
        }

        public IList<T> GetAll()
        {
            return Context.Set<T>().ToList();
        }

        public T GetById(int id)
        {
            return Context.Set<T>().FirstOrDefault(x => x.Id == id);
        }
    }
}